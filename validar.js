function validar (){
    var codigo, titulo, autor, editorial, año;
    codigo = document.getElementById("codigo").value;
    titulo = document.getElementById("titulo").value;
    autor = document.getElementById("autor").value;
    editorial = document.getElementById("editorial").value;
    año = document.getElementById("año").value;

    const expresiones = {
        codigo: /^[a-zA-ZÀ-ÿ\s]{0,5}$/, // cinco caracteres
        titulo: /^[a-zA-ZÀ-ÿ\s]{0,100}$/, // 100 CARACTERES.
        autor: /^[a-zA-ZÀ-ÿ\s]{0,60}$/, // 4 a 12 digitos.
        editorial: /^[a-zA-ZÀ-ÿ\s]{0,30}$/, // 4 a 12 digitos. 
        año: /^.{0,4}$/,
        
    }
    
    if(codigo ==="" || titulo ==="" || autor === "" || editorial === "" || año === ""){
        alert("Todos los campos son obligatorios");
        return false;
    }
    else if(codigo.length>5){
        alert("El codigo es muy largo");
        return false;
  
    }
    else if(!expresiones,test(codigo)){

    }
    else if(titulo.length>100){
        alert("El titulo es muy largo");
        return false;
    }
    else if(!expresiones,test(titulo)){
        
    }
    else if(autor.length>60){
        alert("El autor es muy largo");
        return false;
    }
    else if(!expresiones,test(autor)){
        
    }
    else if(editorial.length>30){
        alert("El editorial es muy largo");
        return false;
    }
    else if(!expresiones,test(editorial)){
        
    }
    else if(año.length>4){
        alert("Solo son numeros");
        return false;
    }
    else if(!expresiones,test(año)){
        
    }
}
