const conexion = require('./cone_base_datos')

module.exports = app => {
    const connect = conexion

    app.post('/Datos_libros', (req, res) => {
        const codigo = req.body.codigo
        const titulo = req.body.titulo
        const autor = req.body.autor
        const editorial = req.body.editorial
        const anio = req.body.anio

        connect.query('insert into libros SET ?', {
            codigo, titulo, autor, editorial, anio
        }, (error, resultado) => {
            res.redirect('/datos')
        })
    })
}